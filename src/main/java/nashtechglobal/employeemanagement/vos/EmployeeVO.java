package nashtechglobal.employeemanagement.vos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import nashtechglobal.employeemanagement.entities.EmployeeEntity;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class EmployeeVO {

    @JsonProperty("employeeId")
    private int employeeId;

    @NonNull
    @JsonProperty("name")
    private String name;

    public EmployeeVO(EmployeeEntity employeeEntity) {
        this.employeeId = employeeEntity.getEmployeeId();
        this.name = employeeEntity.getName();
    }
}

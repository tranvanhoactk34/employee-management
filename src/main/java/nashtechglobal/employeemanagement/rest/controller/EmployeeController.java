package nashtechglobal.employeemanagement.rest.controller;

import nashtechglobal.employeemanagement.entities.EmployeeEntity;
import nashtechglobal.employeemanagement.repository.EmployeeRepository;
import nashtechglobal.employeemanagement.vos.EmployeeVO;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
public class EmployeeController {

    private final EmployeeRepository employeeRepository;

    EmployeeController(EmployeeRepository repository) {
        this.employeeRepository = repository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void initDatabaseEmployees() {
        employeeRepository.save(new EmployeeEntity("Lan"));
        employeeRepository.save(new EmployeeEntity("Mai"));
        employeeRepository.save(new EmployeeEntity("Hoa"));
        employeeRepository.save(new EmployeeEntity("Phong"));
    }

    @GetMapping(path = "employees")
    public List<EmployeeVO> getEmployees() {
        return employeeRepository.findAll().stream().map(EmployeeVO::new).collect(Collectors.toList());
    }

    @GetMapping(path = "employees/{employeeId}")
    public EmployeeVO getEmployee(@PathVariable(name = "employeeId") int employeeId) {
        Function<EmployeeEntity, EmployeeVO> mappingFunc = EmployeeVO::new;
        Optional<EmployeeEntity> employeeEntityOptional = employeeRepository.findById(employeeId);

        return employeeEntityOptional.map(mappingFunc)
                .orElseThrow(() -> new ResourceNotFoundException("Cannot find employee with id: " + employeeId));
    }

    @DeleteMapping(path = "employees/{employeeId}")
    public void deleteEmployee(@PathVariable(name = "employeeId") int employeeId) {
        employeeRepository.deleteById(employeeId);
    }

    @PostMapping(path = "employees")
    public EmployeeEntity createNewEmployee(@RequestBody EmployeeVO employeeVO) {
        EmployeeEntity employeeEntity = new EmployeeEntity(employeeVO.getName());
        return employeeRepository.save(employeeEntity);
    }

    @PutMapping(path = "employees/{employeeId}")
    public EmployeeEntity updateEmployee(@PathVariable(name = "employeeId") int employeeId, @RequestBody EmployeeVO employeeVO) {
        return employeeRepository.findById(employeeId).map(employee -> {
            employee.setName(employeeVO.getName());
            return employeeRepository.save(employee);
        }).orElseGet(() -> {
            EmployeeEntity employeeEntity = new EmployeeEntity();
            employeeEntity.setName(employeeVO.getName());
            return employeeRepository.save(employeeEntity);
        });
    }
}
